#!/bin/sh

envsubst '$PROXY_TARGET' < nginx.conf.template > /etc/nginx/nginx.conf
nginx -g "daemon off;"
