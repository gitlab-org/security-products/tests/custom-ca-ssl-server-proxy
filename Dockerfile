FROM nginx

EXPOSE 443

COPY gitlabcypherpackages.com.bundle.crt /etc/nginx/
COPY gitlabcypherpackages.com.key /etc/nginx/

COPY ca.crt .
COPY nginx.conf nginx.conf.template
COPY cmd.sh /

CMD /cmd.sh
