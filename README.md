# Custom CA SSL Server Proxy

This repository builds a docker image that can be used to test connecting to a TLS server with a self
signed CA certificate. It sets up an nginx reverse proxy to a configurable endpoint.

## Target Configuration

Set the `PROXY_TARGET` env var to the domain you'd like to proxy to.

## Hostname

The hostname is gitlabcypherpackages.com which is a slight nod to Cypher from the matrix.

## Public CA CRT

The ca.crt file in the root of this repo can be added to your bundle of trusted CAs for your system before
connecting to this server as a test for certificates signed by custom CA certificates.

## Example

Here is an example for how to run proxy gitlab.com with this docker image on a docker network named `test`. A
good use case is to do this to point at a go package on gitlabcypherpackages.com which then resolves to
gitlab.com

```
docker run -d --name gitlabcypherpackages.com --net test -e PROXY_TARGET=gitlab.com \
  registry.gitlab.com/gitlab-org/security-products/tests/custom-ca-ssl-server
```
